int pop = 1000;

Object[] sq = new Object[pop];
PImage img;

void setup() {
  //size(1080, 720);
  fullScreen();
  background(0);
  img = loadImage("snowflake.png");

  for (int i = 0; i < pop; ++i) {
    sq[i] = new Object();
  }
}

void draw() {
  background(0);

  for (Object s : sq) {
    s.show();
    s.update();
  }
}

boolean running = true;

void mousePressed() {
  if (running) {
    noLoop();
    running = false;
  } else {
    loop();
    running = true;
  }
}

class Object {
  float x;
  float y; 
  float z;
  float speed;
  float sX;
  float sY;

  Object() {
    x = random(width);
    y = random(500, 1000) * -1.0;
    z = random(0, 10);
    speed = map(z, 0, 10, 2, 20);
    sX = map(z, 0, 10, 5, 15);
    sY = map(z, 0, 10, 5, 15);
  }

  void show() {
    imageMode(CENTER);
    image(img, x, y, sX, sY);
  }

  void update() {

    y += speed;
    speed += map(z, 0, 10, 0.02, 0.1);

    if (y >= height) {
      z = random(0, 10);
      x = random(width);
      y = random(500, 1000) * -1.0;
      speed = map(z, 0, 10, 2, 20);
      sX = map(z, 0, 10, 5, 15);
      sY = map(z, 0, 10, 5, 15);
    }
  }
}
