class Asteroid {
  final int sizeFactor = 30;
  final float speedFactor = 1.1;
  final int lift = -30;

  final float speedMin = 0.5;
  final float speedMax = 2;

  final int maxLife = 4;
  int life;
  int col = 255;
  int colRange;

  PVector location;
  PVector velocity;
  int size;
  float speed;

  float gravity = 0.07;

  Asteroid(float xPos){
    this(xPos, 1);
  }

  Asteroid(float xPos, float speedFactor) {
    location = new PVector(xPos, 0);

    life = (int)random(1, maxLife);
    size = sizeFactor * life;

    //speed = random(speedMin, speedMax);
    speed = speedFactor * life;
    velocity = new PVector(0, speed);

    colRange = (int)255 / life;
  }

  void show() {
    display();
    update();
  }

  void boost(float offset) {
    boost(offset, this.lift);
  }
  
  void boost(float offset, float lift){
    velocity.add(new PVector(offset, lift));
  }

  float getY() {
    return location.y;
  }

  void hit() {
    --life;
    size -= sizeFactor;
    col -= colRange;
  }

  boolean isDead() {
    return life == 0;
  }

  private void display() {
    push();
    strokeWeight(1);
    stroke(0);
    colorMode(HSB);
    fill(255, col, 255);
    ellipse(location.x, location.y, size, size);
    pop();
  }

  private void update() {
    velocity.y += gravity;
    velocity.mult(0.9);
    //velocity.limit(speed);
    location.add(velocity);
  }
}
