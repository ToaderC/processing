package esy.gui;

import processing.event.KeyEvent;

//used on keydown and key up
public class KeyEventArgs extends EventArgs {
  public char keychar;
  public int keycode;
  public boolean control;
  public boolean alt;
  public boolean shift;
  public boolean handled; // same

  public KeyEventArgs(char keychar, int keycode, boolean control, boolean alt, boolean shift) {
    this.keychar = keychar;
    this.keycode = keycode;
    this.control = control;
    this.alt = alt;
    this.shift = shift;
  }

  public KeyEventArgs(KeyEvent event) {
    this.keychar = event.getKey();
    this.keycode = event.getKeyCode();
    this.control = event.isControlDown();
    this.alt = event.isAltDown();
    this.shift = event.isShiftDown();
  }
}
