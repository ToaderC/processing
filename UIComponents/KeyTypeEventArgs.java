package esy.gui;

//should not be able to handle special keys, should trigger only for char or number keys
public class KeyTypeEventArgs extends EventArgs {
  public char keyChar;
  public boolean handled; //should be use to prevent the key to be processed by control if is true, after the event was raised check if true and stop doing anything else, useful for example in TextBox to not insert Handled keys

  /* In this example on a TextBox if a digit is pressed should not be added to textbox because it was already "Handled"
   if (char.IsDigit(e.KeyChar))
   {
   // Set Handled to true to prevent the character from being added to the TextBox
   e.Handled = true;
   Console.WriteLine("Digits are not allowed.");
   }
   */

  public KeyTypeEventArgs(char keyChar) {
    this.keyChar = keyChar;
    handled = false;
  }
}
