package esy.gui;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;
import processing.event.KeyEvent;
import processing.core.PFont;


//TODO: check PGraphics for optimizations of static content and layer drawing
//TODO: Can have a class UI which should always be instantiated and to be the parent of all components, when you create a component you have to add it to UI to be displayed
// like this it will have an array of components and it can manage the order of display, like if I drag a button over another button, the latest one to be displayed on top, or the focused compoenent always on top
// and can be achieved by reordering the list based of a display index
// TODO: like this I can also add a display index or z index to determine the ordering of drawing
// TODO: the initialize is better to be used on UI like ui.Initialize

//TODO: for some components the Text is not used and should add a comment to it
// Can be changed to support array of handlers for each event and call all of them when triggered, like this can add subscribe and unsubscribe an event to handler

//TODO: if want to release add Javadoc comments and documentation for methods
public abstract class Component {

  // Static variables
  protected static PApplet applet;
  //TODO: might need to be private instead of protected
  protected static Component focusedComponent;

  // Constants
  protected static final int DEFAULT_MOUSE_HOVER_DELAY_MS = 500;
  //TODO: throws exception because applet no itinialized, this will work after creating the UI class where it will be initialized
  //TODO: add all the defaults from button here as well after making UI class, like borderColor
  //protected static final int DEFAULT_BACK_COLOR = applet.color(240, 240, 240);

  //Instance variables
  protected TextProvider tp;

  //Properties
  protected String name;
  protected String text;
  protected PVector position;
  protected int sizeX;
  protected int sizeY;
  protected boolean enabled;
  protected boolean visible;
  protected boolean focused;
  protected int backColor;
  protected int mouseHoverDelayMs;
  //TODO: add enum for this
  protected int borderStyle;
  protected int borderColor;
  protected float borderSize;
  protected PFont textFont;
  protected int textColor;
  //TODO: should be added only for specific components, like button
  protected int textAlignHorizontally;
  protected int textAlignVertically;
  protected int textSize;
  protected int zIndex;

  // Flags
  //TODO the flags are not working correctly
  protected boolean flag_isInside = false;
  //TODO: fix later isClicked to create animation when leaves the area while clicked and comes back
  protected boolean flag_isClicked = false;
  protected boolean flag_isMousePressed = false;
  protected boolean flag_isMouseReleased = true;

  // Event handlers
  //Triggers when LEFT click is pressed on button
  protected EventHandler<EventArgs> onClick;

  //Trigegers when mouse is over component and a mouse button is held down
  protected EventHandler<MouseEventArgs> onMouseDown;

  //Trigegers when mouse is over component and a mouse button is released
  protected EventHandler<MouseEventArgs> onMouseUp;

  //Triggers when mouse is moved while inside component, triggers continuously
  protected EventHandler<MouseEventArgs> onMouseMove;

  //Triggers while mouse is pressed then dragged
  protected EventHandler<MouseEventArgs> onMouseDrag;

  //Triggers after a delay if the mouse is still inside
  protected EventHandler<EventArgs> onMouseHover;

  //Triggers instantly when the mouse goes inside
  protected EventHandler<EventArgs> onMouseEnter;

  //Triggers when the mouse goes outside of control
  protected EventHandler<EventArgs> onMouseLeave;

  //Triggers when the key is first pressed
  protected EventHandler<KeyEventArgs> onKeyDown;

  //Triggers when the key is released
  protected EventHandler<KeyEventArgs> onKeyUp;

  //Triggers when the key is typed (characters only)
  protected EventHandler<KeyTypeEventArgs> onKeyType;

  // Private instance variables
  private boolean wasInside = false;
  private int hoverTimerStart = 0;
  private boolean triggeredHover = false;

  public Component() {
    applet.registerMethod("keyEvent", this);
    applet.registerMethod("mouseEvent", this);

    setDefaults();
  }

  public Component(int sizeX, int sizeY) {
    this();
    this.sizeX = sizeX;
    this.sizeY = sizeY;
  }

  // Abstract methods
  protected abstract boolean isInside();
  protected abstract void initializeTextProvider();
  abstract void display();

  // Methods
  static void Initialize(PApplet parent) {
    applet = parent;
  }

  //TODO: add two private or protected event for OnGotFocused and OnLostFocused to handle focused on components
  // Methods update and display package private to be used with EsyGui class
  void update() {
    if (isInside()) {
      flag_isInside = true;

      //MouseEnter
      if (!wasInside) {
        triggerEvent(onMouseEnter, new EventArgs());

        wasInside = true;
        hoverTimerStart = applet.millis();
        triggeredHover = false; // reset flag if triggered event
      }

      //MouseHover
      if (!triggeredHover && (applet.millis() - hoverTimerStart >= mouseHoverDelayMs)) {
        triggerEvent(onMouseHover, new EventArgs());
      }
    } else {

      //Mouse Leave
      if (wasInside) {
        triggerEvent(onMouseLeave, new EventArgs());

        wasInside = false;
        triggeredHover = false;
        flag_isInside = false;
        flag_isMousePressed = false;
        flag_isClicked = false;
      }
    }
  }

  public void mouseEvent(MouseEvent event) {
    if (isInside()) {
      switch (event.getAction()) {
      case MouseEvent.PRESS:
        if (event.getButton() == PApplet.LEFT) {
          flag_isClicked = true;
        }
        flag_isMousePressed = true;
        flag_isMouseReleased = false;
        setFocused(true);

        triggerEvent(onMouseDown, new MouseEventArgs(applet.mouseX, applet.mouseY, applet.mouseButton));
        break;

      case MouseEvent.RELEASE:
        flag_isClicked = false;
        flag_isMousePressed = false;
        flag_isMouseReleased = true;
        triggerEvent(onMouseUp, new MouseEventArgs(applet.mouseX, applet.mouseY, applet.mouseButton));
        break;

      case MouseEvent.CLICK:
        if (event.getButton() == PApplet.LEFT) {
          triggerEvent(onClick, new EventArgs());
        }
        break;

      case MouseEvent.MOVE:
        triggerEvent(onMouseMove, new MouseEventArgs(applet.mouseX, applet.mouseY, applet.mouseButton));
        break;

        //TODO: BUG when clicking and holding from outside and going inside it still calls the drag / fixed with flag_isClicked, still have to check if flags are correct
      case MouseEvent.DRAG:
        flag_isClicked = false;
        if (flag_isMousePressed) {
          triggerEvent(onMouseDrag, new MouseEventArgs(applet.mouseX, applet.mouseY, applet.mouseButton));
        }
        break;
      }
    }
  }

  public void keyEvent(KeyEvent event) {
    if (focused) {
      if (event.getAction() == KeyEvent.PRESS) {
        handleKeyPressEvent(event);
      } else if (event.getAction() == KeyEvent.RELEASE) {
        handleKeyReleaseEvent(event);
      } else if (event.getAction() == KeyEvent.TYPE) {
        handleKeyTypeEvent(event);
      }
    }
  }

  @Override
    public String toString() {
    return name;
  }

  // Getter and Setter methods
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getText() {
    return text;
  }

  /**
   * Gets the text of the component.
   *
   * <p>This property is not used for <specific> component and has no effect.</p>
   ^
   * @return the text of the component
   */

  public void setText(String text) {
    this.text = text;
  }

  public PVector getPosition() {
    return position;
  }

  public void setPosition(PVector position) {
    this.position = position;

    tp.setPosition(position);
  }

  public int getSizeX() {
    return sizeX;
  }

  public void setSizeX(int sizeX) {
    this.sizeX = sizeX;

    tp.setWidth(sizeX);
  }

  public int getSizeY() {
    return sizeY;
  }

  public void setSizeY(int sizeY) {
    this.sizeY = sizeY;

    tp.setHeight(sizeY);
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public boolean isFocused() {
    return focused;
  }

  public void setFocused(boolean focus) {
    if (this.focused == focus) {
      return;
    }

    this.focused = focus;

    if (focus) {
      if (focusedComponent != null) {
        focusedComponent.setFocused(false);
      }
      focusedComponent = this;
    } else {
      //TODO: add event handlers for GotFocus and LostFocus and here trigger LostFocus and in if trigger GotFocus
    }
  }

  public int getBackColor() {
    return backColor;
  }

  public void setBackColor(int backColor) {
    this.backColor = backColor;
  }

  public int getMouseHoverDelayMs() {
    return mouseHoverDelayMs;
  }

  public void setMouseHoverDelayMs(int mouseHoverDelayMs) {
    this.mouseHoverDelayMs = mouseHoverDelayMs;
  }

  public int getBorderStyle() {
    return borderStyle;
  }

  public void setBorderStyle(int borderStyle) {
    this.borderStyle = borderStyle;
  }

  public int getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(int borderColor) {
    this.borderColor = borderColor;
  }

  public float getBorderSize() {
    return borderSize;
  }

  public void setBorderSize(float borderSize) {
    this.borderSize = borderSize;
  }

  public PFont getTextFont() {
    return textFont;
  }

  public void setTextFont(PFont font) {
    this.textFont = font;

    tp.setFont(font);
  }

  public int getTextColor() {
    return textColor;
  }

  public void setTextColor(int textColor) {
    this.textColor = textColor;

    tp.setTextColor(textColor);
  }

  public int getTextAlignHorizontally() {
    return textAlignHorizontally;
  }

  public int getTextAlignVertically() {
    return textAlignVertically;
  }

  public void setTextAlign(int textAlignHorizontally, int textAlignVertically) {
    this.textAlignVertically = textAlignVertically;
    this.textAlignHorizontally = textAlignHorizontally;

    tp.setTextAlign(textAlignHorizontally, textAlignVertically);
  }

  public int getTextSize() {
    return textSize;
  }

  public void setTextSize(int textSize) {
    this.textSize = textSize;
    
    tp.setTextSize(textSize);
  }

  public int getZIndex() {
    return zIndex;
  }

  public void setZIndex(int zIndex) {
    this.zIndex = zIndex;
  }

  // Event handler setters
  public final void setOnClick(EventHandler<EventArgs> onClick) {
    this.onClick = onClick;
  }

  //TODO: implement a tooltip
  public final void setOnMouseHover(EventHandler<EventArgs> onMouseHover) {
    this.onMouseHover = onMouseHover;
  }

  public final void setOnMouseDown(EventHandler<MouseEventArgs> onMouseDown) {
    this.onMouseDown = onMouseDown;
  }

  public final void  setOnMouseUp(EventHandler<MouseEventArgs> onMouseUp) {
    this.onMouseUp = onMouseUp;
  }

  public final void setOnMouseMove(EventHandler<MouseEventArgs> onMouseMove) {
    this.onMouseMove = onMouseMove;
  }

  public final void setOnMouseDrag(EventHandler<MouseEventArgs> onMouseDrag) {
    this.onMouseDrag = onMouseDrag;
  }

  public final void setOnMouseEnter(EventHandler<EventArgs> onMouseEnter) {
    this.onMouseEnter = onMouseEnter;
  }

  public final void setOnMouseLeave(EventHandler<EventArgs> onMouseLeave) {
    this.onMouseLeave = onMouseLeave;
  }

  public final void setOnKeyType(EventHandler<KeyTypeEventArgs> onKeyType) {
    this.onKeyType = onKeyType;
  }

  public final void setOnKeyUp(EventHandler<KeyEventArgs> onKeyUp) {
    this.onKeyUp = onKeyUp;
  }

  public final void setOnKeyDown(EventHandler<KeyEventArgs> onKeyDown) {
    this.onKeyDown = onKeyDown;
  }

  protected <T extends EventArgs> void triggerEvent(EventHandler<T> handler, T args) {
    if (handler != null) {
      handler.handle(this, args);
    }
  }

  protected void handleKeyPressEvent(KeyEvent event) {
    triggerEvent(onKeyDown, new KeyEventArgs(event));
  }

  protected void handleKeyReleaseEvent(KeyEvent event) {
    triggerEvent(onKeyUp, new KeyEventArgs(event));
  }

  protected void handleKeyTypeEvent(KeyEvent event) {
    triggerEvent(onKeyType, new KeyTypeEventArgs(event.getKey()));
  }

  protected void setDefaults() {
    this.enabled = true;
    this.visible = true;
    this.mouseHoverDelayMs = DEFAULT_MOUSE_HOVER_DELAY_MS;
    this.zIndex = 0;
    //this.backColor = DEFAULT_BACK_COLOR;
  }
}
