package esy.gui;

import processing.core.PApplet;
import java.util.ArrayList;


//TODO: maybe create a base class parent component which has the arrayList of components and add, get, remove component methods, and this extends that base class and also for example the container component or groupBox
//TODO: have a read about onPropertyChange in java, to subscribe to focused property of component, move focusedComponent from Component here and make the change here
public final class EsyGui {
  // Static variables
  private Component focusedComponent;

  private ArrayList<Component> componentsArray;

  public EsyGui(PApplet papplet) {
    this.componentsArray = new ArrayList<Component>();
    Component.Initialize(papplet);
  }
    
  public void display() {
    for (Component comp : componentsArray) {
      comp.display();
    }     
  }

  public void update() {
    for (Component comp : componentsArray) {
      comp.update();
    }
  }

  public void addComponent(Component component) {
    componentsArray.add(component);
  }

  //TODO: used to switch between diferent gui's, by default is show set
  public void show() {
  }

  public void hide() {
  }
}
