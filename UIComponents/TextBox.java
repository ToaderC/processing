package esy.gui;
import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;
import processing.event.KeyEvent;

//TODO: can have a blinker class and inject in here, like horizontal or vertical blinker and can have any type of custom blinker
public class TextBox extends Component {

  //Constantin
  protected static int DEFAULT_TEXTBOX_COLOR = applet.color(255, 255, 255);
  protected static int DEFAULT_TEXT_COLOR = applet.color(0);
  protected static int DEFAULT_PLACEHOLDER_COLOR = applet.color(150);
  protected static int DEFAULT_CARET_BLINK_DELAY_MS = 500;
  protected static int DEFAULT_TEXTSIZE_PADDING = 10;
  protected static int DEFAULT_PADDING = 5;

  //TODO: move this into separate key class
  protected static char BACKSPACE_CHAR = '\b';
  protected static char DEL_CHAR = '\u007F';
  protected static char ENTER_CHAR = '\n';
  protected static int HOME_CODE = 36;
  protected static int END_CODE = 35;

  private int cursorPosition;
  private boolean displayCaret;
  private int caretBlinkTime;
  private int caretLastBlinkTime;
  private boolean isCtrlPressed;

  //TODO: add setters and getters
  protected String placeHolder;


  protected KeyTypeEventArgs keyTypeEventArgs;
  protected int placeHolderTextColor;

  public TextBox(PVector position, int sizeX, int sizeY, String name) {
    this(position, sizeX, sizeY, name, "");
  }

  public TextBox(PVector position, int sizeX, int sizeY, String name, String placeHolder) {
    this.position = position;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    this.name = name;
    this.placeHolder = placeHolder;

    initializeTextProvider();
  }

  @Override
    public boolean isInside() {
    return ((position.x <= applet.mouseX && position.x + sizeX >= applet.mouseX) && (position.y <= applet.mouseY && position.y + sizeY >= applet.mouseY));
  }

  @Override
    protected void initializeTextProvider() {
    this.tp = new TextProvider(position, sizeX, sizeY, applet);
    tp.setTextAlign(PApplet.LEFT, PApplet.CENTER);
    tp.setTextMode(TextMode.CLIPTOSIZE);
    tp.setPaddingHorizontal(DEFAULT_PADDING);
    tp.setTextSize(this.sizeY - DEFAULT_TEXTSIZE_PADDING);
  }

  @Override
    protected void setDefaults() {
    super.setDefaults();

    this.backColor = DEFAULT_TEXTBOX_COLOR;
    this.placeHolderTextColor = DEFAULT_PLACEHOLDER_COLOR;
    this.caretBlinkTime = DEFAULT_CARET_BLINK_DELAY_MS;

    this.text = "";
    this.cursorPosition = 0;
    this.displayCaret = true;
    this.caretLastBlinkTime = applet.millis();
    this.isCtrlPressed = false;
  }

  @Override
    public void keyEvent(KeyEvent event) {
    super.keyEvent(event);

    if (!focused) {
      return;
    }
    if (event.getAction() != KeyEvent.PRESS) {
      return;
    }

    int keyCode = event.getKeyCode();

    isCtrlPressed = event.isControlDown();

    if (keyCode == PApplet.BACKSPACE) {
      if (cursorPosition > 0) {
        text = text.substring(0, cursorPosition - 1) + text.substring(cursorPosition);
        cursorPosition--;
        handleTextOverflow();
        //updateDisplayText(text);
      }
    } else if (keyCode == PApplet.DELETE) {
      if (cursorPosition < text.length()) {
        text = text.substring(0, cursorPosition) + text.substring(cursorPosition + 1);
        handleTextOverflow();
        //updateDisplayText(text);
      }
    } else if (keyCode == PApplet.LEFT) {
      if (cursorPosition > 0) {
        cursorPosition--;
        displayCaret = true;
      }
    } else if (keyCode == PApplet.RIGHT) {
      if (cursorPosition < text.length()) {
        cursorPosition++;
        displayCaret = true;
      }
    } else if (keyCode == HOME_CODE) {
      cursorPosition = 0;
    } else if (keyCode == END_CODE) {
      cursorPosition = text.length();
    }
  }

  @Override
    protected void handleKeyTypeEvent(KeyEvent event) {
    char keyChar = event.getKey();

    //TODO: might need to move it in Component class and never trigger TYPE event for this keys
    if (keyChar == BACKSPACE_CHAR || keyChar == DEL_CHAR || keyChar == ENTER_CHAR || isCtrlPressed) {
      return;
    }

    keyTypeEventArgs = new KeyTypeEventArgs(keyChar);

    triggerEvent(onKeyType, keyTypeEventArgs);

    if (!keyTypeEventArgs.handled) {
      updateText(keyChar);
    }
  }

  @Override
    void update() {
    super.update();

    if (flag_isInside) {
      applet.cursor(PApplet.TEXT);
    } else {
      applet.cursor(PApplet.ARROW);
    }

    if (flag_isClicked) {
      setCursorToMousePosition(applet.mouseX);
    }
  }

  @Override
    void display() {
    applet.pushStyle();
    applet.fill(backColor);
    applet.rect(position.x, position.y, sizeX, sizeY);

    handleCaret();
    tp.displayText(text);

    applet.popStyle();
  }

  //TODO: all this methods probably should be private, but will see when trying to create new TextBox
  protected void handleCaret() {
    if (applet.millis() - caretLastBlinkTime > caretBlinkTime) {
      displayCaret = !displayCaret;
      caretLastBlinkTime = applet.millis();
    }

    if (displayCaret) {
      displayCaret();
    }
  }

  protected void displayCaret() {
    if (!focused) {
      return;
    }

    float cursorX = tp.getTextWidth(text.substring(0, cursorPosition));

    applet.line(position.x + DEFAULT_PADDING + cursorX, position.y + 5,
      position.x + DEFAULT_PADDING + cursorX, position.y + sizeY - 5);
  }

  protected void updateText(char keyChar) {
    text = text.substring(0, cursorPosition) + keyChar + text.substring(cursorPosition);
    cursorPosition++;
    handleTextOverflow();
  }

  //TODO: BUG: when text overflows and caret is somewhere in middle, the text should be pushed only to right until caret reaches the end, only then push to the left
  private void handleTextOverflow() {
    float textWidth = tp.getTextWidth(text);

    if (textWidth > sizeX - DEFAULT_PADDING * 2) {
      tp.setTextAlign(PApplet.RIGHT, PApplet.CENTER);
    } else {
      tp.setTextAlign(PApplet.LEFT, PApplet.CENTER);
    }
  }

  private void setCursorToMousePosition(float mx) {
    float relativeX = mx - (position.x + DEFAULT_PADDING);
    float currentX = 0;
    cursorPosition = 0;

    for (int i = 0; i < text.length(); i++) {
      float charWidth = tp.getTextWidth(Character.toString(text.charAt(i)));

      if (currentX + charWidth / 2 > relativeX) {
        break;
      }

      currentX += charWidth;
      cursorPosition++;
    }
  }
}
