package esy.gui;

public interface EventHandler<T extends EventArgs> {
  void handle(Object sender, T e);
}
