package esy.gui;
import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;
import processing.event.KeyEvent;


/*TODO: Things to add
 border
 border height
 color
 animation when clicked
 for TextBox default mouse change to typing
 for TextBox implement placeholder text which is shown on focus only until you type
 implement visible
 implement enabled
 */

// TODO: can add a ButtonBase class which has all the button functionality and normal shape and behaviour and a class Button which will override the display only, is inside would be for square in ButtonBase
// Then can easly add new Button design by extending ButtonBase

//TODO: maybe use interfaces and implement for Text and Picture because some components use text and implements and other not same as for backgroundImage, this can implement background image, but TextBox does not have
//TODO: implement default bahavior on ENTER pressed to call the click event
public class Button extends Component implements IButtonComponent {

  // Constants
  protected static int DEFAULT_BUTTON_COLOR = applet.color(225, 225, 225);
  protected static int DEFAULT_BORDER_COLOR = applet.color(173, 173, 173);
  protected static int DEFAULT_BORDER_SIZE = 1;

  //TODO: This are button specific, remove the comment after moving component specific
  protected static int DEFAULT_BUTTON_HOVER_COLOR = applet.color(229, 241, 251);
  protected static int DEFAULT_BUTTON_CLICK_COLOR = applet.color(204, 228, 247);
  protected static int DEFAULT_BORDER_HOVER_COLOR = applet.color(0, 120, 215);
  protected static int DEFAULT_BORDER_FOCUSED_SIZE = 3;

  //TODO: maybe also add BackgroundImage for component
  //TODO: also add hover back color and hover border color and hover border size
  // Instance variables
  protected boolean animation = true;

  public Button(PVector position, int sizeX, int sizeY, String name) {
    this(position, sizeX, sizeY, name, "");
  }

  public Button(PVector position, int sizeX, int sizeY, String name, String text) {
    this.position = position;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    this.name = name;
    this.text = text;

    initializeTextProvider();
  }

  //Getter and Setter methods
  public boolean hasAnimation() {
    return animation;
  }

  public void setAnimation(boolean animation) {
    this.animation = animation;
  }

  // Override Methods
  @Override
    public boolean isInside() {
    return ((position.x <= applet.mouseX && position.x + sizeX >= applet.mouseX) && (position.y <= applet.mouseY && position.y + sizeY >= applet.mouseY));
  }

  @Override
    protected void initializeTextProvider() {
    this.tp = new TextProvider(position, sizeX, sizeY, applet);
    tp.setTextAlign(PApplet.CENTER, PApplet.CENTER);
    tp.setTextMode(TextMode.CLIPTOLETTER);
    tp.setPaddingHorizontal(5);
    tp.setPaddingVertical(2);
  }

  @Override
    protected void setDefaults() {
    super.setDefaults();

    //TODO: this visible is for test only, remove
    //TODO: add this in the base component after creating UI class
    this.visible = false;
    this.backColor = DEFAULT_BUTTON_COLOR;
    this.borderColor = DEFAULT_BORDER_COLOR;
    this.borderSize = DEFAULT_BORDER_SIZE;
  }

  //TODO: this is needed for TextBox, later remove from here
  @Override
    public void keyEvent(KeyEvent event) {
    super.keyEvent(event);

    if (event.getAction() == KeyEvent.TYPE && focused) {
      if (event.getKey() == 'g') {
        PApplet.println("Inside button " + toString());
      }
    }
  }

  @Override
    void update() {
    super.update();
  }

  @Override
    void display() {
    applet.pushStyle();
    animation();
    render();
    applet.popStyle();
  }

  @Override
    public void performClick() {
    triggerEvent(onClick, new EventArgs());
  }

  // Private methods
  //TODO: separate animation logic from render so it could be easly overriden, like this the button will have a default animation logic in display, in subclass can override the logic method, call base logic and add some more, and after call render
  //TODO: can separate like drawBody, drawBorder, drawText, drawAnimation
  protected void render() {
    applet.fill(backColor);
    applet.rect(position.x, position.y, sizeX, sizeY);
    tp.displayText(text);
  }

  protected void animation() {
    //TODO: animation should decide for color changing on hover and click
    //TODO: borderStyle should decide if noStroke or stroke

    applet.stroke(borderColor);
    applet.strokeWeight(borderSize);

    //TODO: BUG if using mouse drag and moving a component over the other, on hover both components will trigger and change color also while clicking and I think takes events of latest one drawn, should be fixed in UI with Z index
    //TODO: the order should be implemented in UI class with arrayList of components
    //TODO: BUG also while dragging a component over another one will trigger color change, should add a flag somehow that one component is being dragged and disable everything else for other components if != this object
    //in Windows Forms when moving a component over another one with mouseMove event, we cursor reaching other component the previous one will stop moving, remove hover color like is not used and the new component will trigger color change
    //because is hover now
    if (animation) {
      if (focused) {
        borderSize = DEFAULT_BORDER_FOCUSED_SIZE;
        borderColor = DEFAULT_BORDER_HOVER_COLOR;
      } else {
        borderSize = DEFAULT_BORDER_SIZE;
        borderColor = DEFAULT_BORDER_COLOR;
      }

      if (flag_isInside) {
        borderSize = DEFAULT_BORDER_SIZE;
        borderColor = DEFAULT_BORDER_HOVER_COLOR;
        backColor = DEFAULT_BUTTON_HOVER_COLOR;
      } else {
        backColor = DEFAULT_BUTTON_COLOR;
      }

      if (flag_isClicked) {
        backColor = DEFAULT_BUTTON_CLICK_COLOR;
      }
    }
  }
}
