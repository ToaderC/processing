package esy.gui;

import processing.core.PApplet;
import processing.core.PVector;
import processing.core.PFont;
import processing.core.PGraphics;

enum TextMode {
  NORMAL,
    CLIPTOSIZE,
    CLIPTOLETTER,
}


//TODO: implement new behavior, clip to word if does not fit and are multiple words, if there is only 1 word which does not fit then clip to letter
//TODO: BUGS, needs testing
class TextProvider {
  private PApplet applet;
  private final int DEFAULT_TEXT_SIZE = 12;
  private final int DEFAULT_TEXT_COLOR = 0xFF000000;
  private PGraphics pg;
  private PVector position;
  private int width;
  private int height;
  private float paddingHorizontal;
  private float paddingVertical;
  private int textSize;
  private TextMode textMode;
  private PFont font;
  private int textColor;
  private int textAlignHorizontally;
  private int textAlignVertically;

  public TextProvider(PVector position, int width, int height, PApplet applet) {
    this.applet = applet;
    this.position = position;
    this.width = width;
    this.height = height;
    this.paddingHorizontal = 0;
    this.paddingVertical = 0;
    this.textSize = DEFAULT_TEXT_SIZE;
    this.textMode = TextMode.NORMAL;
    this.textColor = DEFAULT_TEXT_COLOR;
    pg = applet.createGraphics(width, height);
  }

  public void displayText(String text) {
    if (text == null || text.isEmpty()) {
      return;
    }

    pg.beginDraw();
    pg.clip(0, 0, width - paddingHorizontal, height - paddingVertical);

    setProperties();

    float textWidth = pg.textWidth(text);

    //TODO: test how growing mode works with label, when setting a background to the label if text and label extends correctly, implement growing on vertical scale as well
    //TODO: also update the growing on vertical scale

    // Growing text without a fixed size
    if (textMode == TextMode.NORMAL) {
      handleNormalMode(text, textWidth);
    } else {
      handleClippingModes(text, textWidth);
    }

    pg.noClip();
    pg.endDraw();

    applet.image(pg, position.x, position.y);
  }

  public void setPosition(PVector position) {
    this.position = position;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public float getPaddingHorizontal() {
    return paddingHorizontal;
  }

  public void setPaddingHorizontal(float padding) {
    this.paddingHorizontal = padding;
  }

  public float getPaddingVertical() {
    return paddingVertical;
  }

  public void setPaddingVertical(float padding) {
    this.paddingVertical = padding;
  }

  public int getTextSize() {
    return textSize;
  }

  public void setTextSize(int textSize) {
    this.textSize = textSize;
  }

  public void setTextMode(TextMode mode) {
    this.textMode = mode;
  }

  public void setFont(PFont font) {
    this.font = font;
  }

  public void setTextColor(int textColor) {
    this.textColor = textColor;
  }

  public void setTextAlign(int textAlignHorizontally, int textAlignVertically) {
    this.textAlignHorizontally = textAlignHorizontally;
    this.textAlignVertically = textAlignVertically;
  }

  public float getTextWidth(String text) {
    if (text.length() <= 0) {
      return 0;
    }

    applet.pushStyle();
    applet.textSize(this.textSize);
    float textWidth = applet.textWidth(text);
    applet.popStyle();

    return textWidth;
  }

  private void handleNormalMode(String text, float textWidth) {
    pg = applet.createGraphics((int)(textWidth + paddingHorizontal), height); // TODO: implement growing on vertical scale
    pg.beginDraw();
    setProperties();
    pg.textAlign(PApplet.LEFT, textAlignVertically);
    pg.text(text, paddingHorizontal, getHeightPosition());
  }

  private void handleClippingModes(String text, float textWidth) {
    if (textWidth <= width - 2 * paddingHorizontal) {
      // Text fits inside the button
      pg.text(text, getWidthPosition(), getHeightPosition());
    } else {
      String displayString = text;

      if (textMode == TextMode.CLIPTOLETTER) {
        displayString = trimTextToFit(text, width - 2 * paddingHorizontal);
      }
      pg.text(displayString, getWidthPosition(), getHeightPosition());
    }
  }

  private String trimTextToFit(String text, float maxWidth) {
    int low = 0;
    int high = text.length();
    int mid;

    while (low < high) {
      mid = (low + high + 1) / 2;

      String substring = text.substring(0, mid);
      if (pg.textWidth(substring) <= maxWidth) {
        low = mid;
      } else {
        high = mid - 1;
      }
    }

    return text.substring(0, low);
  }

  private float getTextHeight(String text) {
    float ascent = pg.textAscent();
    float descent = pg.textDescent();

    return ascent + descent;
  }

  private float getWidthPosition() {
    switch (textAlignHorizontally) {
    case PApplet.RIGHT:
      return width - paddingHorizontal;
    case PApplet.CENTER:
      return width / 2;
    default:
      return paddingHorizontal;
    }
  }

  private float getHeightPosition() {
    switch (textAlignVertically) {
    case PApplet.TOP:
      return paddingVertical;
    case PApplet.BOTTOM:
      return height - paddingVertical;
    default:
      return height / 2;
    }
  }

  private void setProperties() {
    if (font != null) {
      pg.textFont(font);
    }

    pg.background(255, 0);
    pg.textSize(textSize);
    pg.fill(textColor);
    pg.textAlign(textAlignHorizontally, textAlignVertically);
  }
}
