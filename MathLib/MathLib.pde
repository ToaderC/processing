//Tests for the Math lib

PVector a;
PVector b;
PVector c;
float r;

void setup(){
  size(720, 720);
  strokeWeight(3);
  
  a = new PVector(100, 300);
  b = new PVector(600, 200);
  r = 50;
}

void draw(){
  background(255);
  c = new PVector(mouseX, mouseY);
  
  line(a.x, a.y, b.x, b.y);
  noFill();
  if(Math.collisionLineCircle(a,b,c,r)){
    fill(127,100);
  }
  ellipse(c.x, c.y, r*2, r*2);
}
