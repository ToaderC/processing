class Node {
  PVector pos;
  Node parent;
  ArrayList<Node> nodes = new ArrayList<Node>();

  Node(PVector position) {
    this(position, null);
  }

  Node(PVector position, Node parent) {
    this.pos = position;
    this.parent = parent;
  }

  void addToSelf(Node node) {
    nodes.add(node);
  }
}
