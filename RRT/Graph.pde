class Graph {
  ArrayList<Node> nodes;
  Node firstNode;
  PVector target;
  int step;
  color pointColor;
  color lineColor;
  color firstNodeColor;
  final int targetRadius = 15;
  boolean targetReached = false;

  Graph(int drawStep) {
    this(drawStep, new PVector(random(width), random(height)));
  }

  Graph(int drawStep, PVector target) {
    nodes = new ArrayList<Node>();
    this.target = target;
    this.step = drawStep;
    this.pointColor = color(255, 0, 0);
    this.lineColor = color(255);
    this.firstNodeColor = color(0, 255, 0);
  }

  void show() {
    fill(0, 255, 0);
    ellipse(target.x, target.y, targetRadius*2, targetRadius*2);
    for (Node i : nodes) {
      noStroke();
      if (i == firstNode) {
        fill(firstNodeColor);
        ellipse(i.pos.x, i.pos.y, 15, 15);
      } else {
        fill(pointColor);
        ellipse(i.pos.x, i.pos.y, 10, 10);
      }
      for (Node j : i.nodes) {
        fill(pointColor);
        stroke(lineColor);
        strokeWeight(2);
        line(i.pos.x, i.pos.y, j.pos.x, j.pos.y);
      }
    }
    
    if(targetReached) {
      stroke(0, 255, 0);
      strokeWeight(3);
      Node last = nodes.get(nodes.size() - 1);
      Node parent = last.parent;
      while(parent != null) {
        line(last.pos.x, last.pos.y, parent.pos.x, parent.pos.y);
        last = parent;
        parent = last.parent;
      }
    }
  }

  void addNode(PVector pos) {
    if (nodes.size() == 0) {
      firstNode = new Node(pos);
      nodes.add(firstNode);
      checkTargetReached(firstNode);
    } else {
      Node closest = getClosest(pos);
      PVector close = new PVector(closest.pos.x, closest.pos.y);
      PVector temp = new PVector(close.x, close.y);
      temp.sub(pos);
      temp.limit(step);
      close.sub(temp);

      Node newNode = new Node(close, closest);
      closest.addToSelf(newNode);
      nodes.add(newNode);
      //checkTargetReached(newNode);
      checkTargetReached(close, closest.pos);
    }
  }

  Node getClosest(PVector toThis) {
    float dst = width*height;
    Node result = new Node(toThis);
    for (Node n : nodes) {
      float dstN = dist(n.pos.x, n.pos.y, toThis.x, toThis.y);
      if (dstN < dst) {
        dst = dstN;
        result = n;
      }
    }

    return result;
  }

  void checkTargetReached(Node node) {
    if (isOnTarget(node.pos)) {
      targetReached = true;
    }
  }

  void checkTargetReached(PVector prev, PVector next) {
    if (reachedTarget(prev, next)) {
      targetReached = true;
      println("reached target");
    }
  }

  boolean reachedTarget(PVector prev, PVector next) {
    return Math.collisionLineCircle(prev, next, target, targetRadius);
  }

  boolean isOnTarget(PVector pos) {
    return ((pow(pos.x - target.x, 2) + pow(pos.y - target.y, 2)) < (pow(targetRadius, 2)));
  }
  
  void reset(){
    nodes.clear();
    targetReached = false;
  }
}
