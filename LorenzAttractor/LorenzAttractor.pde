import peasy.*;

float sigma = 10;
float ro = 28;
float beta = 8.0 / 3.0;

float x = 1;
float y = 1;
float z = 1;
float dt = 0.01;

ArrayList<PVector> points = new ArrayList<PVector>();

PeasyCam cam;

void setup() {
  frameRate(100);
  size(800, 600, P3D);
  background(0);
  colorMode(HSB);
  cam = new PeasyCam(this, 1000);
}

void draw() {
  background(0);

  float dx = (sigma * (y - x)) * dt;
  float dy = (x * ( ro - z) - y) * dt;
  float dz = (x * y - beta * z) * dt;

  x = x + dx;
  y = y + dy;
  z = z + dz;

  points.add(new PVector(x, y, z));

  stroke(255);
  scale(5);
  noFill();

  int col = 0;

  beginShape();
  for (PVector point : points) {
    stroke(col, 255, 255);
    vertex(point.x, point.y, point.z);

    //applyShaking(point, 0.01);
    
    col += 1;

    if (col > 255) {
      col = 0;
    }
  }
  endShape();
}


void applyShaking(PVector point) {
  applyShaking(point, 0.3);
}

void applyShaking(PVector point, float level) {
  PVector shaking = PVector.random3D();
  shaking.mult(level);
  point.add(shaking);
}
