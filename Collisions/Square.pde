class Square {
  PVector position;
  PVector velocity;
  float momentum;
  color col;
  int size;

  Square(float x, float y, int size, color col) {
    position = new PVector(x, y);
    velocity = new PVector(0, 3);//random(2, 5));
    velocity.mult(2);
    this.col = col;
    this.size = size;
    momentum = size* .1;
  }

  void update() {
    position.add(velocity);
    println(position.mag());
    velocity.limit(50);
  }

  void display() {
    stroke(0);
    strokeWeight(2);
    fill(col);
    rectMode(CENTER);
    rect(position.x, position.y, size, size);
  }

  void checkBounds() {
    if (position.y + size / 2 > height || 
      position.y - size / 2 < 0) {
      velocity.y *= -1;
    }
  }

  boolean collision(Square other) {
    if (position.x < other.position.x + other.size + 0.1 &&
      position.x + size > other.position.x + 0.1 &&
      position.y < other.position.y + other.size + 0.1 &&
      size + position.y > other.position.y + 0.1) {
      return true;
    }
    //float yDif = abs(position.y - other.position.y);
    //if (yDif < size / 2 + other.size / 2) {
    //  println(yDif + "(" + position.y + " " + other.position.y + ")");
    //  return true;
    //}
    return false;
  }


  //PVector dist = PVector.sub(position, other.position);

  //float magnitude = dist.mag();
  //float minDist = size;

  //if(magnitude < minDist){
  //  other.velocity.y *= -1;
  //  velocity.y *= -1;
  //}
}

class SquareMover {
  final int pixelsDif = 100;
  final int squareSize = 60;

  int mousePrevX = 0;

  ArrayList<Square> squares = new ArrayList<Square>();

  void update() {
    for (int i = 0; i < squares.size(); ++i) {
      Square temp = squares.get(i);
      temp.display();
      temp.update(); 
      temp.checkBounds();

      for (int j = i + 1; j < squares.size(); ++j) {
        Square sq = squares.get(j);
        if (sq.collision(temp)) {
          sq.velocity.y *= -1;
          temp.velocity.y *= -1;
        }
      }
    }
  }

  void mousePressed() {
    mousePrevX = mouseX;
    addSquare(mouseX, mouseY);
  }

  void mouseDragged() {
    int difX = abs(mouseX - mousePrevX);

    if (difX > pixelsDif) {
      mousePrevX = mouseX;
      addSquare(mouseX, mouseY);
    }
  }

  public void addSquare(float x, float y) {  
    squares.add(new Square(x, y, squareSize, color(random(255), random(255), random(255))));
  }
}
