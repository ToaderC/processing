PGraphics canvas;
PGraphics preview;
PGraphics toolbar;

final int CIRCLE_TOOL = 0;
final int PEN_TOOL = 1;
PImage circleIcon;
PImage penIcon;
int currentTool = PEN_TOOL;
int initialX;
int initialY;
float currentWeight;
color currentColor;

void setup() {
  size(1080, 720);

  canvas = createGraphics(1080, 720);
  preview = createGraphics(1080, 720);
  toolbar = createGraphics(80, 720);

  circleIcon = loadImage("circle.png");
  penIcon = loadImage("pen.png");

  currentColor = #000000;
  currentWeight = 3;

  background(255);
}

void draw() {
  toolbar.beginDraw();
  toolbar.stroke(0);
  toolbar.fill(127);
  toolbar.rect(0, 0, 90, height);

  button(circleIcon, 5, 5, 70, 70, CIRCLE_TOOL);
  button(penIcon, 5, 80, 70, 70, PEN_TOOL);
  toolbar.endDraw();
  
  background(255);
  image(canvas, 0, 0);
  image(preview, 0, 0);
  image(toolbar, 0, 0);
}

void button(PImage icon, int x, int y, int w, int h, int tool) {
  if (mouseHover(x, y, w, h)) {
    toolbar.stroke(255, 0, 0);
  } else {
    toolbar.stroke(255);
  }

  if (currentTool == tool) {
    toolbar.fill(200, 0, 0);
  } else {
    toolbar.fill(120);
  }

  toolbar.rect(x, y, w, h);
  toolbar.image(icon, x, y, w, h);
}

boolean mouseHover(int x, int y, int w, int h) {
  return mouseX > x && mouseX < x + w && mouseY > y && mouseY < y + h;
}

boolean onCanvas() {
  return mouseX > 80;
}

void mousePressed() {
  initialX = mouseX;
  initialY = mouseY;
}

void mouseDragged() {
  if (currentTool == PEN_TOOL) {
    canvas.beginDraw();
    canvas.strokeWeight(currentWeight);
    canvas.stroke(currentColor);
    canvas.line(pmouseX, pmouseY, mouseX, mouseY);
    canvas.endDraw();
  } else if (currentTool == CIRCLE_TOOL) {
    preview.beginDraw();
    if (onCanvas()) {
      preview.clear();
    }
    preview.ellipseMode(CORNER);
    preview.stroke(currentColor);
    preview.strokeWeight(currentWeight);
    preview.noFill();
    preview.ellipse(initialX, initialY, mouseX - initialX, mouseY - initialY);
    preview.ellipseMode(CENTER);
    preview.endDraw();
  }
}

void mouseReleased() {
  if (mouseHover(5, 5, 70, 70)) {
    currentTool = CIRCLE_TOOL;
  } else if (mouseHover(5, 80, 70, 70)) {
    currentTool = PEN_TOOL;
  }
  
  if (currentTool == CIRCLE_TOOL) {
    canvas.beginDraw();
    canvas.ellipseMode(CORNER);
    canvas.stroke(currentColor);
    canvas.strokeWeight(currentWeight);
    canvas.noFill();
    canvas.ellipse(initialX, initialY, mouseX - initialX, mouseY - initialY);
    canvas.ellipseMode(CENTER);
    canvas.endDraw();
  }
}
