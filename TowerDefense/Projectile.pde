class Projectile {
  ProjectileStats stats;
  PVector target;

  //TODO: target currently not used. Can be used if collision detection will be moved here to check with only one enemy instead of all
  Projectile(ProjectileStats projectileStats, PVector target) {
    this.stats = projectileStats;
    this.target = target;
  }

  void display() {
    stroke(255);
    strokeWeight(7);
    point(stats.position.x, stats.position.y);
  }

  void update() {
    stats.position.add(stats.velocity);
  }
}
