class GameManager {
  GameStats stats;
  Tower tower;
  WaveGenerator waveGenerator;
  GameUI ui;

  ArrayList<Enemy> enemies;
  ArrayList<Projectile> projectiles;

  GameManager() {
    this.stats = new GameStats();
    this.tower = new TowerProvider().getInitialTower();
    this.enemies = new ArrayList<Enemy>();
    this.projectiles = new ArrayList<Projectile>();
  }

  void initialize() {
    this.waveGenerator = new WaveGenerator(enemies, tower.stats.position);
    this.ui = new GameUI(stats);
  }

  //TODO: Can be improvement if Projectile stores an instance of Enemy and then I can check to that enemy not with all the enemies
  void update() {
    checkEnemiesAndProjectiles();
    checkProjectilesOutOfScreen();

    tower.update();
    tower.shoot(projectiles);

    waveGenerator.update();

    ui.update();
    ui.display();
  }

  void display() {
    tower.display();

    if (tower.isDead()) {
      noLoop();
      println("dead");
    }

    for (Enemy enemy : enemies) {
      enemy.display();
    }

    for (Projectile pj : projectiles) {
      pj.display();
    }
  }

  private boolean enemyInsideTowerRange(Enemy enemy) {
    if (Math.collisionRectangleCircle(enemy.stats.position, enemy.stats.size, enemy.stats.size, tower.stats.position, tower.stats.range, CENTER)) {
      return true;
    }

    return false;
  }

  private void checkEnemiesAndProjectiles() {
    for (int i = enemies.size() - 1; i >= 0; i--) {
      Enemy enemy = enemies.get(i);
      enemy.update();

      //Check if enemy inside of tower range
      if (enemyInsideTowerRange(enemy)) {
        tower.addInRange(enemy);
      }

      for (int j = projectiles.size() - 1; j >= 0; j--) {
        Projectile pj = projectiles.get(j);

        //Check collision projectile <-> enemy
        if (Math.collisionPointRectangle(pj.stats.position, enemy.stats.position, enemy.stats.size, enemy.stats.size, CENTER)) {
          projectiles.remove(j);

          enemy.takeDamage(pj.stats.damage);

          //Enemy died
          if (enemy.isDead()) {
            enemies.remove(i);
            tower.removeEnemy(enemy);
            stats.credits++;
          }

          break;
        }
      }

      // Enemy reached Tower
      if (Math.collisionRectangleRectangle(enemy.stats.position, enemy.stats.size, enemy.stats.size, tower.stats.position, tower.stats.size, tower.stats.size, CENTER)) {
        enemy.stats.velocity = new PVector(0, 0);

        tower.takeDamage(enemy.stats.damage / frameRate);

        //TODO: normalize position
      }
    }
  }

  private void checkProjectilesOutOfScreen() {
    for (int i = projectiles.size() - 1; i >= 0; i--) {
      Projectile pj = projectiles.get(i);
      pj.update();

      //Check if bullet is outside of screen width and height
      if (!Math.collisionPointRectangle(pj.stats.position, new PVector(0, 0), width, height)) {
        projectiles.remove(i);
      }
    }
  }
}
