static class Math {

  /**
   * Check collision between a line and a circle
   * @param a First point of the line
   * @param b Second point of the line
   * @param c Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the line is colliding with the circle
   */
  static boolean collisionLineCircle(PVector a, PVector b, PVector c, float r) {
    return collisionLineCircle(a.x, a.y, b.x, b.y, c.x, c.y, r);
  }


  /**
   * Check collision between a line and a circle
   * @param x1,y1 First point of the line
   * @param x2,y2 Second point of the line
   * @param cx,cy Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the line is colliding with the circle
   */
  static boolean collisionLineCircle(float x1, float y1, float x2, float y2, float cx, float cy, float r) {
    // is either end INSIDE the circle?
    // if so, return true immediately
    boolean inside1 = collisionPointCircle(x1, y1, cx, cy, r);
    boolean inside2 = collisionPointCircle(x2, y2, cx, cy, r);
    if (inside1 || inside2) return true;

    // get length of the line
    float distX = x1 - x2;
    float distY = y1 - y2;
    float len = sqrt( (distX*distX) + (distY*distY) );

    // get dot product of the line and circle
    float dot = ( ((cx-x1)*(x2-x1)) + ((cy-y1)*(y2-y1)) ) / pow(len, 2);

    // find the closest point on the line
    float closestX = x1 + (dot * (x2-x1));
    float closestY = y1 + (dot * (y2-y1));

    // is this point actually on the line segment?
    // if so keep going, but if not, return false
    boolean onSegment = collisionLinePoint(x1, y1, x2, y2, closestX, closestY);
    if (!onSegment) return false;

    // optionally, draw a circle at the closest
    // point on the line
    //fill(255,0,0);
    //noStroke();
    //ellipse(closestX, closestY, 20, 20);

    // get distance to closest point
    distX = closestX - cx;
    distY = closestY - cy;
    float distance = sqrt( (distX*distX) + (distY*distY) );

    if (distance <= r) {
      return true;
    }
    return false;
  }


  /**
   * Check collision between a point and a circle
   * @param p Point position
   * @param c Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the point is colliding with the circle
   */
  static boolean collisionPointCircle(PVector p, PVector c, float r) {
    return collisionPointCircle(p.x, p.y, c.x, c.y, r);
  }


  /**
   * Check collision between a point and a circle
   * @param px,py Point position
   * @param cx,cy Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the point is colliding with the circle
   */
  static boolean collisionPointCircle(float px, float py, float cx, float cy, float r) {

    // get distance between the point and circle's center
    // using the Pythagorean Theorem
    float distX = px - cx;
    float distY = py - cy;
    float distance = sqrt((distX*distX) + (distY*distY));

    // if the distance is less than the circle's
    // radius the point is inside!
    if (distance <= r) {
      return true;
    }
    return false;
  }


  /**
   * Check collision between a line and a point
   * @param a First point of the line
   * @param b Second point of the line
   * @param p Point position
   * @return boolean Returns true if the line is colliding with the point
   */
  static boolean collisionLinePoint(PVector a, PVector b, PVector p) {
    return collisionLinePoint(a.x, a.y, b.x, b.y, p.x, p.y);
  }


  /**
   * Check collision between a line and a point
   * @param x1,y1 First point of the line
   * @param x2,y2 Second point of the line
   * @param px,py The point to check against
   * @return boolean Returns true if the line is colliding with the point
   */
  static boolean collisionLinePoint(float x1, float y1, float x2, float y2, float px, float py) {

    // get distance from the point to the two ends of the line
    float d1 = dist(px, py, x1, y1);
    float d2 = dist(px, py, x2, y2);

    // get the length of the line
    float lineLen = dist(x1, y1, x2, y2);

    // since floats are so minutely accurate, add
    // a little buffer zone that will give collision
    float buffer = 0.1;    // higher # = less accurate

    // if the two distances are equal to the line's
    // length, the point is on the line!
    // note we use the buffer here to give a range,
    // rather than one #
    if (d1+d2 >= lineLen-buffer && d1+d2 <= lineLen+buffer) {
      return true;
    }
    return false;
  }


  /**
   * Check collision between a rectangle and a circle
   * @param rectPos Rectangle position
   * @param rectW,rectH Rectangle width and height
   * @param circlePos Circle center position
   * @param circleR Radius of the circle
   */
  static boolean collisionRectangleCircle(PVector rectPos, float rectW, float rectH, PVector circlePos, float circleR) {
    return collisionRectangleCircle(rectPos.x, rectPos.y, rectW, rectH, circlePos.x, circlePos.y, circleR, CORNER);
  }


  /**
   * Check collision between a rectangle and a circle
   * @param rectPos Rectangle position
   * @param rectW,rectH Rectangle width and height
   * @param circlePos Circle center position
   * @param circleR Radius of the circle
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionRectangleCircle(PVector rectPos, float rectW, float rectH, PVector circlePos, float circleR, int rectMode) {
    return collisionRectangleCircle(rectPos.x, rectPos.y, rectW, rectH, circlePos.x, circlePos.y, circleR, rectMode);
  }


  /**
   * Check collision between a rectangle and a circle
   * @param rectX,rectY Rectangle position
   * @param rectW,rectH Rectangle width and height
   * @param circleX,circleY Circle center position
   * @param circleR Radius of the circle
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionRectangleCircle(float rectX, float rectY, float rectW, float rectH, float circleX, float circleY, float circleR, int rectMode) {
    float closestX, closestY;

    if (rectMode == CENTER) {
      closestX = constrain(circleX, rectX - rectW / 2, rectX + rectW / 2);
      closestY = constrain(circleY, rectY - rectH / 2, rectY + rectH / 2);
    } else {
      closestX = constrain(circleX, rectX, rectX + rectW);
      closestY = constrain(circleY, rectY, rectY + rectH);
    }

    float distanceX = circleX - closestX;
    float distanceY = circleY - closestY;
    float distance = sqrt(distanceX * distanceX + distanceY * distanceY);

    return distance < circleR;
  }


  /**
   * Check collision between a point and a rectangle
   * @param point Point position
   * @param rectPos Rectangle position
   * @param rectW,rectH Rectangle width and height
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionPointRectangle(PVector point, PVector rectPos, float rectW, float rectH) {
    return collisionPointRectangle(point.x, point.y, rectPos.x, rectPos.y, rectW, rectH, CORNER);
  }


  /**
   * Check collision between a point and a rectangle
   * @param point Point position
   * @param rectPos Rectangle position
   * @param rectW,rectH Rectangle width and height
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionPointRectangle(PVector point, PVector rectPos, float rectW, float rectH, int rectMode) {
    return collisionPointRectangle(point.x, point.y, rectPos.x, rectPos.y, rectW, rectH, rectMode);
  }


  /**
   * Check collision between a point and a rectangle
   * @param pointX,pointY Point position
   * @param rectX,rectY Rectangle position
   * @param rectW,rectH Rectangle width and height
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionPointRectangle(float pointX, float pointY, float rectX, float rectY, float rectW, float rectH, int rectMode) {
    if (rectMode == CENTER) {
      return (pointX >= rectX - rectW / 2) && (pointX <= rectX + rectW / 2) &&
        (pointY >= rectY - rectH / 2) && (pointY <= rectY + rectH / 2);
    } else {
      return (pointX >= rectX) && (pointX <= rectX + rectW) &&
        (pointY >= rectY) && (pointY <= rectY + rectH);
    }
  }


  /**
   * Check collision between a rectangle and another rectangle
   * @param rect1 First rectangle position
   * @param w1,h1 First rectangle width and height
   * @param rect2 Second rectangle position
   * @param w2,h2 Second rectangle width and height
   */
  static boolean collisionRectangleRectangle(PVector rect1, float w1, float h1, PVector rect2, float w2, float h2) {
    return collisionRectangleRectangle(rect1.x, rect1.y, w1, h1, rect2.x, rect2.y, w2, h2, CORNER);
  }


  /**
   * Check collision between a rectangle and another rectangle
   * @param rect1 First rectangle position
   * @param w1,h1 First rectangle width and height
   * @param rect2 Second rectangle position
   * @param w2,h2 Second rectangle width and height
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionRectangleRectangle(PVector rect1, float w1, float h1, PVector rect2, float w2, float h2, int rectMode) {
    return collisionRectangleRectangle(rect1.x, rect1.y, w1, h1, rect2.x, rect2.y, w2, h2, rectMode);
  }


  /**
   * Check collision between a rectangle and another rectangle
   * @param x1,y1 First rectangle position
   * @param w1,h1 First rectangle width and height
   * @param x2,y2 Second rectangle position
   * @param w2,h2 Second rectangle width and height
   * @rectMode rectMode for how the rectangle is being drawn. Supports default value and CENTER
   */
  static boolean collisionRectangleRectangle(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2, int rectMode) {
    if (rectMode == CENTER) {
      x1 -= w1 / 2;
      y1 -= h1 / 2;
      x2 -= w2 / 2;
      y2 -= h2 / 2;
    }

    // Calculate rectangle boundaries
    float left1 = x1;
    float right1 = x1 + w1;
    float top1 = y1;
    float bottom1 = y1 + h1;

    float left2 = x2;
    float right2 = x2 + w2;
    float top2 = y2;
    float bottom2 = y2 + h2;

    // Check for overlap along x-axis and y-axis
    boolean overlapX = (right1 >= left2) && (left1 <= right2);
    boolean overlapY = (bottom1 >= top2) && (top1 <= bottom2);

    return overlapX && overlapY;
  }
}
