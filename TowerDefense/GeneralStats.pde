//TODO: can include dmg and remove hp so can be used in projectile as well //<>//
class Stats {
  PVector position;
  float hp;
  float size;
  float damage;

  Stats(
    PVector position,
    float hp,
    float size,
    float damage) {
    this.position = position;
    this.hp = hp;
    this.size = size;
    this.damage = damage;
  }
}

// ------------- Tower -----------------

class TowerStats extends Stats {
  float range;
  float fireRate;

  TowerStats(
    PVector position,
    float range,
    float fireRate,
    float damage,
    float hp,
    float size) {
    super(position, hp, size, damage);
    this.range = range;
    this.fireRate = fireRate;
  }
}

class TowerProvider {
  Tower getInitialTower() {
    PVector screenCenter = new PVector(width / 2, height / 2);
    float range = 300;
    float fireRate = 2;
    float damage = 1.0;
    float hp = 10.0;
    float size = 25.0;

    return new Tower(new TowerStats(
      screenCenter,
      range,
      fireRate,
      damage,
      hp,
      size));
  }
}

// ------------- Enemy -----------------

class EnemyStats extends Stats {
  PVector velocity;
  float speed;

  EnemyStats(
    PVector position,
    PVector velocity,
    float speed,
    float hp,
    float size,
    float damage) {
    super(position, hp, size, damage);
    this.velocity = velocity;
    this.speed = speed;
  }
}

class EnemyProvider {
  Enemy getEnemy(PVector towerPosition) {
    float speed = 4.0;
    float size = 20;
    float hp = 6;
    float dmg = 1.0;
    PVector position = getStartPosition(size);
    PVector velocity = getVelocity(position, towerPosition, speed);

    return new Enemy(new EnemyStats(
      position,
      velocity,
      speed,
      hp,
      size,
      dmg),
      towerPosition);
  }

  Enemy getEnemy(PVector towerPosition, float speed, float hp, float damage) {
    float size = 20;
    PVector position = getStartPosition(size);
    PVector velocity = getVelocity(position, towerPosition, speed);

    return new Enemy(new EnemyStats(
      position,
      velocity,
      speed,
      hp,
      size,
      damage),
      towerPosition);
  }

  Enemy getEnemyStub(PVector position, PVector towerPosition, float speed, float hp, float damage) {
    float size = 20;
    PVector velocity = getVelocity(position, towerPosition, speed);

    return new Enemy(new EnemyStats(
      position,
      velocity,
      speed,
      hp,
      size,
      damage),
      towerPosition);
  }

  private PVector getVelocity(PVector startPosition, PVector towerPosition, float speed) {
    PVector velocity = PVector.sub(towerPosition, startPosition);
    velocity.normalize();
    velocity.mult(speed);

    return velocity;
  }

  private PVector getStartPosition(float size) {
    PVector pos;

    switch((int)random(4)) {
      //top
    case 0:
      pos = new PVector(random(width), -size);
      break;
      //right
    case 1:
      pos = new PVector(width + size, random(height));
      break;
      //down
    case 2:
      pos = new PVector(random(width), height + size);
      break;
      //left
    default:
      pos = new PVector(-size, random(height));
      break;
    }

    return pos;
  }
}

// ------------- Projectile -----------------

class ProjectileStats {
  PVector position;
  PVector velocity;
  float speed;
  float damage;

  ProjectileStats(
    PVector position,
    PVector velocity,
    float speed,
    float damage) {
    this.position = position;
    this.velocity = velocity;
    this.speed = speed;
    this.damage = damage;
  }
}

class ProjectileProvider {
  Projectile getProjectile(PVector from, PVector to, float damage) {
    float speed = 10;
    PVector velocity = PVector.sub(to, from);
    velocity.normalize();
    velocity.mult(speed);

    ProjectileStats stats = new ProjectileStats(
      from.copy(),
      velocity,
      speed,
      damage);

    return new Projectile(stats, to);
  }
}

// ------------- Game Stats -----------------

class GameStats {
  int credits;
  
  GameStats() {
    this.credits = 400;
  }
}
