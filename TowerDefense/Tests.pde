class WaveStub implements IWave {
  EnemyProvider enemyProvider;
  float spawnRate;
  int totalEnemies;
  int enemiesSpawned;
  float lastSpawnTime;
  PVector towerPosition;
  ArrayList<Enemy> enemies;
  float speed;
  PVector position;

  WaveStub(ArrayList<Enemy> enemies, PVector towerPosition) {
    this.towerPosition = towerPosition;
    this.totalEnemies = 3;
    this.spawnRate = 3500;
    this.enemiesSpawned = 0;
    this.lastSpawnTime = 0;
    this.enemyProvider = new EnemyProvider();
    this.towerPosition = towerPosition;
    this.enemies = enemies;
  }


  void update() {
    switch(enemiesSpawned) {
    case 0:
      speed = 0.7;
      position = new PVector(0, height / 2);
      break;
    case 1:
      speed = 1.0;
      position = new PVector(width, height / 2);
      break;
    case 2:
      speed = 1.2;
      spawnRate = 4000;
      position = new PVector(width / 2, 0);
    }
    
    if (millis() - lastSpawnTime > spawnRate && enemiesSpawned < totalEnemies) {
      float hp = 20;
      float damage = 0;
      enemies.add(enemyProvider.getEnemyStub(position, towerPosition, speed, hp, damage));
      enemiesSpawned++;
      lastSpawnTime = millis();
    }
  }

  boolean isOver() {
    return totalEnemies <= enemiesSpawned;
  }
}
