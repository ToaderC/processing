class Enemy implements Comparable<Enemy> {
  EnemyStats stats;
  PVector towerPosition;
  float distanceToTower;

  Enemy(EnemyStats enemyStats, PVector towerPosition) {
    this.stats = enemyStats;
    this.towerPosition = towerPosition;
    this.distanceToTower = PVector.dist(this.stats.position, towerPosition);
  }

  void update() {
    stats.position.add(stats.velocity);
    distanceToTower -= stats.velocity.mag();
  }

  void display() {
    noFill();
    stroke(0, 230, 0);
    rectMode(CENTER);
    rect(stats.position.x, stats.position.y, stats.size, stats.size);
  }

  int compareTo(Enemy other) {
    return Float.compare(distanceToTower, other.distanceToTower);
  }

  @Override
    public String toString() {
    int identifier = System.identityHashCode(this);

    return String.format("ID: %d, Distance: %.2f", identifier, distanceToTower);
  }

  float distance() {
    return distanceToTower;
  }

  void takeDamage(float damage) {
    stats.hp -= damage;
  }

  boolean isDead() {
    return stats.hp <= 0;
  }
}
