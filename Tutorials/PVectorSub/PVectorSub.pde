PVector center;

void setup() {
  size(500, 300);
}

void draw() {
  background(255);
  strokeWeight(2);
  stroke(0);
  noFill();

  translate(width/2, height/2);
  ellipse(0, 0, 4, 4);

  PVector mouse = new PVector(mouseX, mouseY);
  center =  new PVector(width/2, height/2);

  //substract from mouse the center
  //distance in straight line between center and mouse position
  mouse.sub(center);

  /*
  center.sub(mouse);
   distance in straight line between center and mouse but in opposite direction
   */
  stroke(0, 255, 0);
  line(0, 0, mouse.x, mouse.y);

  //with blue are vectors from origin to center and mouse position
  //with green is mouse PVector after center subtraction 
  //with red is mouse PVector before center subtraction
  
  //after translation the mouse.sub(center) is necessary to draw the line from new center to mouse position
  example();
  
  //draw a rectangle with width equal to length of mouse vector after subtract
  //length from center point to mouse position
  float mag = mouse.mag();
   
  stroke(0);
  fill(255,0,0);
  rect(0,0,-mag,30);
}

void example() {
  stroke(0, 0, 255);
  line(-width / 2, -height / 2, 0, 0);
  println(mouseX, mouseY);

  PVector mouse = new PVector(mouseX, mouseY);
  
  stroke(255, 0, 0);
  line(0, 0, mouse.x, mouse.y);

  mouse.sub(center);
  
  stroke(0, 0, 255);
  line(-width / 2, -height / 2, mouse.x, mouse.y);
}
