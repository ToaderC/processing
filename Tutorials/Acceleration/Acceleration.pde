Ball ball;

void setup(){
  size(1280, 720);
  background(255);
  
  ball = new Ball();
}

void draw(){
  background(255);
  ball.display();
  ball.update();
  ball.bounds();
}

void mousePressed(){
  ball.mousePressed();
}
