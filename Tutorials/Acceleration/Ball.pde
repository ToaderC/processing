class Ball {
  final float radius = 50;
  boolean pressed = false;

  PVector location;
  PVector velocity;
  PVector acceleration;
  PVector pressedLocation;
  PVector originalPossition;
  float originalDistance;

  Ball() {
    location = new PVector(width/2, height/2);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0.0, 0);
  }

  void display() {
    stroke(0);
    strokeWeight(1.5);
    fill(210);
    ellipse(location.x, location.y, radius, radius);
  }

  void update() {
    if (pressed) {

      fill(0);
      ellipse(pressedLocation.x, pressedLocation.y, 10, 10);
      ellipse(originalPossition.x, originalPossition.y, 10, 10);

      PVector mouse = new PVector(pressedLocation.x, pressedLocation.y);
      mouse.sub(location);

      float m = mouse.mag();
      acceleration = new PVector(mouse.x, mouse.y);

      if (m >= originalDistance/2 - velocity.mag()) {
        acceleration.setMag(0.05);
      } else {
        acceleration.setMag(-0.05);
      }

      if (m < 1) {
        acceleration = new PVector(0, 0);
        velocity = new PVector(0, 0);
      }

      println(velocity);

      velocity.add(acceleration);
      location.add(velocity);
      //velocity.limit(5);
    }
  }

  void updateMoving() {
    PVector mouse = new PVector(mouseX, mouseY);
    mouse.sub(location);
    mouse.setMag(0.5);
    acceleration = mouse;

    velocity.add(acceleration);
    location.add(velocity);
    velocity.limit(5);
  }

  void bounds() {
    if (location.x > width)
      location.x = 0;
    if (location.x < 0)
      location.x = width;
    if (location.y > height)
      location.y = 0;
    if (location.y < 0)
      location.y = height;
  }

  void mousePressed() {
    pressedLocation = new PVector(mouseX, mouseY);

    PVector dist = new PVector(mouseX, mouseY);
    dist.sub(location);

    originalDistance = dist.mag();

    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);

    originalPossition = new PVector(location.x, location.y);

    pressed = true;
  }
}
