class Tile {
  private float _size;
  private color _initialColor;
  private Piece _piece;
  private Position _positionInMatrix;
  private Position _positionOnScreen;


  public Tile(float xMatrix, float yMatrix, float xScreen, float yScreen, float size, color col) {
    this(new Position(xMatrix, yMatrix), new Position(xScreen, yScreen), size, col);
  }
  
  public Tile(Position matrix, Position screen, float size, color col) {
    _initialColor = col;
    _size = size;
    _positionInMatrix = matrix;
    _positionOnScreen = screen;
  }

  public void display() {
    noStroke();
    fill(_initialColor);
    rect(_positionOnScreen.x, _positionOnScreen.y, _size, _size);
  }

  public void update() {
  }
}
