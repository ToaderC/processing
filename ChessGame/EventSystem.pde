class EventArgs {
  public EventArgs() {
  }

  public EventArgs Empty() {
    return new EventArgs();
  }
}

class MouseEventArgs extends EventArgs {
  private MouseButton _button;
  private float _x;
  private float _y;

  public MouseEventArgs() {
  }

  public MouseButton getButton() {
    return _button;
  }

  public float getX() {
    return _x;
  }

  public float getY() {
    return _y;
  }

  public Position getPosition() {
    return new Position(_x, _y);
  }
}

enum MouseButton {
  LEFT, 
    RIGHT, 
    MIDDLE,
}
